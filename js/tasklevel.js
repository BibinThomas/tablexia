inheritPrototype(Tasklevel, PIXI.Text);

inheritPrototype(Tasklevel, PIXI.Application);

function Tasklevel() {

    this.gender_text = [];

    this.m_uppercloth_text = [];
    this.m_lowercloth_text = [];
    this.w_uppercloth_text = [];
    this.w_lowercloth_text = [];

    this.gender_text.push(new PIXI.Text("പുരുഷന്റെ", {
        fill: "black",
        font: "bold 26px Chilanka"
    }));
    this.gender_text.push(new PIXI.Text("സ്ത്രീയുടെ", {
        fill: "black",
        font: "bold 26px Chilanka"
    }));


    this.m_uppercloth_text.push(new PIXI.Text("ഷർട്ടിന്റെ", {
        fill: "black",
        font: "bold 26px Chilanka"
    }));
    this.m_uppercloth_text.push(new PIXI.Text("സറ്ററിന്റെ", {
        fill: "black",
        font: "bold 26px Chilanka"
    }));


    this.m_lowercloth_text.push(new PIXI.Text("പാന്റിന്റെ", {
        fill: "black",
        font: "bold 26px Chilanka"
    }));
    this.m_lowercloth_text.push(new PIXI.Text("മുണ്ടിന്റെ", {
        fill: "black",
        font: "bold 26px Chilanka"
    }));


    this.w_uppercloth_text.push(new PIXI.Text("ഷർട്ടിന്റെ", {
        fill: "black",
        font: "bold 26px Chilanka"
    }));
    this.w_uppercloth_text.push(new PIXI.Text("ബ്ലൗസിന്റെ", {
        fill: "black",
        font: "bold 26px Chilanka"
    }));

    this.w_lowercloth_text.push(new PIXI.Text("പാന്റിന്റെ", {
        fill: "black",
        font: "bold 26px Chilanka"
    }));
    this.w_lowercloth_text.push(new PIXI.Text("മിഡിയുടെ", {
        fill: "black",
        font: "bold 26px Chilanka"
    }));

};

Tasklevel.prototype = {

    constructor: Tasklevel,

    easyLevel: function() {

        this.activity_robbery_rule_txt = new PIXI.Text("ഒരു വ്യക്തിയുടെ\t\t\t\t\t\t\t\t\t\t\t\t\t \t\t\t\t\t\t\t\t\t\t\t\t\t വസ്ത്രം\t\t\t\t\t\t\t\t\t\t\t\t\tഉം \n\n \t\t\t\t\t\t\t\t\t\t\t\t\t വസ്ത്രം \t\t\t\t\t\t\t\t\t\t\t\t\tഉം \n\n ആണെങ്കിൽ അയാൾ കള്ളൻ ആണ് ", {

            fill: "grey",
            font: "bold 26px Chilanka",

        });
        parent.activity_robbery_rule_txt = new PIXI.Text("ഒരു വ്യക്തിയുടെ മുകളിലത്തെ വസ്ത്രം\t\t\t\t\t\t\t\t\t\t\t\t\tഉം \n\n താഴത്തെ വസ്ത്രം \t\t\t\t\t\t\t\t\t\t\t\t\tഉം \n\n ആണെങ്കിൽ അയാൾ കള്ളൻ ആണ് ", {

            fill: "grey",
            font: "bold 26px Chilanka",

        });


       activity_obj.positioning_any_sprite(activity_obj.activity_robbery_rule_txt, activity_obj.xPos, activity_obj.yPos, 1);
       //first color
       activity_obj.positioning_any_sprite(activity_obj.color_array[activity_obj.num], activity_obj.xPos+175, activity_obj.yPos-60, 1);

       // second color
       activity_obj.positioning_any_sprite(activity_obj.color_array[parent.num1], activity_obj.xPos-30, activity_obj.yPos, 1);

       activity_obj.activity_clueCntr.addChild(activity_obj.color_array[activity_obj.num]);
       activity_obj.activity_clueCntr.addChild(activity_obj.color_array[activity_obj.num1]);
       activity_obj.activity_clueCntr.addChild(activity_obj.activity_robbery_rule_txt);
       app.stage.addChild(activity_obj.activity_clueCntr);


    },

    mediumLevel: function() {

        this.activity_robbery_rule_txt = new PIXI.Text("ഒരു \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t \t\t\t\t\t\t\t\t\t\t നിറം\t\t\t\t\t\t\t\t\t\t\t\t\tഉം \n\n \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t നിറം \t\t\t\t\t\t\t\t\t\tഉം \n\n ആണെങ്കിൽ അയാൾ കള്ളൻ ആണ് ", {

            fill: "grey",
            font: "bold 26px Chilanka",

        });
        //main text
        activity_obj.positioning_any_sprite(this.activity_robbery_rule_txt, activity_obj.xPos, activity_obj.yPos, 1);

        //first color
        activity_obj.positioning_any_sprite(activity_obj.color_array[activity_obj.num], activity_obj.xPos + 160, activity_obj.yPos - 60, 1);

        // second color
        activity_obj.positioning_any_sprite(activity_obj.color_array[activity_obj.num1], activity_obj.xPos-10, activity_obj.yPos, 1);


        // array index for upper and lower cloth text
        this.uppercloth_index = activity_obj.random_number_generator(this.m_uppercloth_text.length);
        this.lowercloth_index = activity_obj.random_number_generator(this.m_uppercloth_text.length);

        //if it is a man
        if (findrobber_obj.gender_array[activity_obj.gen_num] == "m") {

            //gender text
            activity_obj.positioning_any_sprite(this.gender_text[0], activity_obj.xPos - 140, activity_obj.yPos - 60, 1);

            //top dress
            activity_obj.positioning_any_sprite(this.m_uppercloth_text[this.uppercloth_index], activity_obj.xPos - 10, activity_obj.yPos - 60, 1);

            //bottom dress
            activity_obj.positioning_any_sprite(this.m_lowercloth_text[this.lowercloth_index], activity_obj.xPos - 185, activity_obj.yPos, 1);

            activity_obj.activity_clueCntr.addChild(this.gender_text[0]);
            activity_obj.activity_clueCntr.addChild(this.m_uppercloth_text[this.uppercloth_index]);
            activity_obj.activity_clueCntr.addChild(this.m_lowercloth_text[this.lowercloth_index]);

        }

        //if it is a women
        else if (findrobber_obj.gender_array[activity_obj.gen_num] == "w") {

            //gender text
            activity_obj.positioning_any_sprite(this.gender_text[1], activity_obj.xPos - 130, activity_obj.yPos - 60, 1);
            //top dress
            activity_obj.positioning_any_sprite(this.w_uppercloth_text[this.uppercloth_index], activity_obj.xPos - 10, activity_obj.yPos - 60, 1);

            //bottom dress
            activity_obj.positioning_any_sprite(this.w_lowercloth_text[this.lowercloth_index], activity_obj.xPos - 185, activity_obj.yPos, 1);

            activity_obj.activity_clueCntr.addChild(this.gender_text[1]);
            activity_obj.activity_clueCntr.addChild(this.w_uppercloth_text[this.uppercloth_index]);
            activity_obj.activity_clueCntr.addChild(this.w_lowercloth_text[this.lowercloth_index]);


        }
        activity_obj.activity_clueCntr.addChild(this.activity_robbery_rule_txt);
        activity_obj.activity_clueCntr.addChild(activity_obj.color_array[activity_obj.num]);
        activity_obj.activity_clueCntr.addChild(activity_obj.color_array[activity_obj.num1]);

        app.stage.addChild(activity_obj.activity_clueCntr);


    },

    hardLevel: function() {

        this.activity_robbery_rule_txt = new PIXI.Text("ഒരു വ്യക്തിയുടെ മുകളിലത്തെ വസ്ത്രം\t\t\t\t\t\t\t\t\t\t\t\t\tഉം \n\n താഴത്തെ വസ്ത്രം \t\t\t\t\t\t\t\t\t\t\t\t\tഉം ആണെങ്കിൽ\n\n അയാൾക്ക്‌ ശേഷം വരുന്ന വ്യക്തി കള്ളൻ ആണ്", {

            fill: "grey",
            font: "bold 26px Chilanka",

        });
        //main text
        activity_obj.positioning_any_sprite(this.activity_robbery_rule_txt, activity_obj.xPos, activity_obj.yPos, 1);

        //first color
        activity_obj.positioning_any_sprite(activity_obj.color_array[activity_obj.num], activity_obj.xPos + 175, activity_obj.yPos - 60, 1);

        // second color
        activity_obj.positioning_any_sprite(activity_obj.color_array[activity_obj.num1], activity_obj.xPos - 30, activity_obj.yPos, 1);

        this.uppercloth_index = activity_obj.random_number_generator(this.m_uppercloth_text.length);
        this.lowercloth_index = activity_obj.random_number_generator(this.m_uppercloth_text.length);


        activity_obj.activity_clueCntr.addChild(this.activity_robbery_rule_txt);
        activity_obj.activity_clueCntr.addChild(activity_obj.color_array[activity_obj.num]);
        activity_obj.activity_clueCntr.addChild(activity_obj.color_array[activity_obj.num1]);

        app.stage.addChild(activity_obj.activity_clueCntr);


    }


    }
