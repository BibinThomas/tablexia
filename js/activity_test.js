function inheritPrototype(childObject, parentObject) {
    var copyOfParent = Object.create(parentObject.prototype);
    copyOfParent.constructor = childObject;
    childObject.prototype = copyOfParent;
}

inheritPrototype(Activity, PIXI.Sprite);

function Activity() {

    this.activity_loadCntr = new PIXI.Container();

    this.instructionCntr = new PIXI.Container();
    this.instructionCntr.visible = false;

    this.activity_clueCntr = new PIXI.Container();
    this.activity_clueCntr.visible = false;

    this.maskingCntr = new PIXI.Container();

    this.xPos = (window.innerWidth) / 2;
    this.yPos = (window.innerHeight) / 2;

    //resources for common activity load page

    this.background = new PIXI.Sprite(PIXI.Texture.fromImage('resources/screen/background.jpg'));

    this.maskingSprite = new PIXI.Sprite(PIXI.Texture.fromImage('resources/screen/mask.png'));

    this.progressbar = new PIXI.Sprite(PIXI.Texture.fromImage('resources/screen/progressbar.png'));

    this.startbutton_pressed = new PIXI.Sprite(PIXI.Texture.fromImage('resources/screen/startbutton_pressed.png'));

    this.startbutton = new PIXI.Sprite(PIXI.Texture.fromImage('resources/screen/startbutton.png'));

    this.title = new PIXI.Sprite(PIXI.Texture.fromImage('resources/screen/title.png'));

    this.difficulty_seekbar_bg = new PIXI.Sprite(PIXI.Texture.fromImage('resources/screen/difficultyseekbar_background.png'));

    this.difficulty_seekbar_easy = new PIXI.Sprite(PIXI.Texture.fromImage('resources/screen/difficultyseekbar_thumb_easy.png'));

    this.difficulty_seekbar_medium = new PIXI.Sprite(PIXI.Texture.fromImage('resources/screen/difficultyseekbar_thumb_medium.png'));

    this.difficulty_seekbar_hard = new PIXI.Sprite(PIXI.Texture.fromImage('resources/screen/difficultyseekbar_thumb_hard.png'));


    //resources for common uses
    this.understand_button_pressed_texture = PIXI.Texture.fromImage('resources/global/tablexiabutton_blank_green_pressed.png');
    this.understand_button_unpressed_texture = PIXI.Texture.fromImage('resources/global/tablexiabutton_blank_green_unpressed.png');

    this.understand_button_pressed_green_texture = PIXI.Texture.fromImage('resources/global/tablexiabutton_blank_green_pressed.9.png');
    this.understand_button_unpressed_green_texture = PIXI.Texture.fromImage('resources/global/tablexiabutton_blank_green_unpressed.9.png');
    this.understand_button_pressed_blue_texture = PIXI.Texture.fromImage('resources/global/tablexiabutton_blank_blue_pressed.9.png');
    this.understand_button_unpressed_blue_texture = PIXI.Texture.fromImage('resources/global/tablexiabutton_blank_blue_unpressed.9.png');
    this.stats_texture = PIXI.Texture.fromImage('resources/global/stats.png');


    //resources for common instruction page

    this.common_bg = new PIXI.Sprite(PIXI.Texture.fromImage('resources/global/table.png'));
    this.dialogue_square = new PIXI.Sprite(PIXI.Texture.fromImage('resources/global/signature_background.png'));
    this.rule_message = new PIXI.Sprite(PIXI.Texture.fromImage('resources/common/gamescreen/rulemessage_paper_title.png'));

    this.tile = new PIXI.Sprite(PIXI.Texture.fromImage('resources/global/tile.png'));

    //resources for activity clue activity_loadingpage
    this.dialogue_square1 = new PIXI.Sprite(PIXI.Texture.fromImage('resources/global/rule_background.png'));

    this.difficulty_text = new PIXI.Text("ഈസി\t\t\t\t\t\t\tമീഡിയം\t\t\t\t\t\tഹാർഡ് ", {

        fill: "black",
        font: "bold 26px Chilanka",

    });

    this.play_again_text = new PIXI.Text("കളിക്കുക" ,{ fill: "black", font: "bold 22px Chilanka" });
    this.exit_text = new PIXI.Text("ക്വിറ്റ്‌" ,{ fill: "black", font: "bold 22px Chilanka" });

};

Activity.prototype = {

    constructor: Activity,

    hover_effect: function(p, up) {

        up
            .on('pointerover', hover_button)
            .on('pointerout', unhover_button)

        function hover_button() {

            up.alpha = 0;
            p.alpha = 1;


        }

        function unhover_button() {

            p.alpha = 0;
            up.alpha = 1;

        }

    },

    positioning_to_center: function(img) {

        this.background.anchor.set(0.5, 0.5);
        this.background.position.set(this.xPos, this.yPos);

    },

    make_interactivebutton: function(bttn) {

        bttn.interactive = true;
        bttn.accessible = true;
        bttn.buttonMode = true;

    },

    positioning_any_sprite: function(img, x, y, scl) {

        img.anchor.set(0.5, 0.5);
        img.position.set(x, y);
        img.scale.set(scl)

    },

    activity_loadingpage: function() {

      this.activity_loadCntr.visible = true;

        //activity page images
        this.positioning_any_sprite(this.title, this.xPos, this.yPos - 150, .8);
        this.positioning_any_sprite(this.startbutton, this.xPos, this.yPos, .9);
        this.positioning_any_sprite(this.startbutton_pressed, this.xPos, this.yPos, .9);
        this.positioning_any_sprite(this.progressbar, this.xPos, this.yPos + 250, .7);
        this.positioning_any_sprite(this.difficulty_seekbar_bg, this.xPos, this.yPos + 160, 1.2);
        this.positioning_any_sprite(this.difficulty_seekbar_easy, this.xPos - 130, this.yPos + 160, 1);
        this.positioning_any_sprite(this.difficulty_seekbar_medium, this.xPos, this.yPos + 160, 1);
        this.positioning_any_sprite(this.difficulty_seekbar_hard, this.xPos + 130, this.yPos + 160, 1);
        this.positioning_any_sprite(this.difficulty_text, this.xPos, this.yPos + 200, 1);

        this.startbutton.alpha = 1;
        this.startbutton_pressed.alpha = 0;

        this.make_interactivebutton(this.startbutton);

        this.hover_effect(this.startbutton_pressed, this.startbutton);

        this.make_interactivebutton(this.difficulty_seekbar_easy);
        this.make_interactivebutton(this.difficulty_seekbar_medium);
        this.difficulty_seekbar_medium.alpha = 0;

        this.make_interactivebutton(this.difficulty_seekbar_hard);
        this.difficulty_seekbar_hard.alpha = 0;

        parent = this;

        this.task_level = 1;

        // PIXI.sound.add('bird', 'resources/common/alarm.mp3');
        // PIXI.sound.play('bird');

        var sound_path = 'resources/common/alarm.mp3'
        var sound_name = 'loading'
        this.game_sounds(sound_path, sound_name);


        this.startbutton.click = function() {

          var sound_path = 'resources/common/tuk_1.mp3'
          var sound_name = 'easy_default'
          parent.game_sounds(sound_path, sound_name);

            parent.common_instruction();

        }

        this.difficulty_seekbar_easy.click = function() {

            var sound_path = 'resources/common/tuk_1.mp3'
            var sound_name = 'easy'
            parent.game_sounds(sound_path, sound_name);

            parent.difficulty_seekbar_medium.alpha = 0;
            parent.difficulty_seekbar_easy.alpha = 1;
            parent.difficulty_seekbar_hard.alpha = 0;

            parent.task_level = 1;

            parent.startbutton.click = function() {
                var sound_name = 'start_easy'
                var sound_path = 'resources/common/tuk_1.mp3'
                parent.game_sounds(sound_path, sound_name);
                parent.common_instruction();

            }
        }

        this.difficulty_seekbar_medium.click = function() {

            var sound_path = 'resources/common/tuk_1.mp3'
            var sound_name = 'medium'
            parent.game_sounds(sound_path, sound_name);

            parent.difficulty_seekbar_medium.alpha = 1;
            parent.difficulty_seekbar_easy.alpha = 0;
            parent.difficulty_seekbar_hard.alpha = 0;

            parent.task_level = 2;

            parent.startbutton.click = function() {

              var sound_path = 'resources/common/tuk_1.mp3'
              var sound_name = 'start_medium'
              parent.game_sounds(sound_path, sound_name);

              parent.common_instruction();


            }
        }

        this.difficulty_seekbar_hard.click = function() {

            var sound_path = 'resources/common/tuk_1.mp3'
            var sound_name = 'hard'
            parent.game_sounds(sound_path, sound_name);

            parent.difficulty_seekbar_hard.alpha = 1;
            parent.difficulty_seekbar_easy.alpha = 0;
            parent.difficulty_seekbar_medium.alpha = 0;

            parent.task_level = 3;

            parent.startbutton.click = function() {

              var sound_path = 'resources/common/tuk_1.mp3'
              var sound_name = 'start_hard'
              parent.game_sounds(sound_path, sound_name);

                parent.common_instruction();

            }

        }

        //activity load container
        this.activity_loadCntr.addChild(this.background);

        this.activity_loadCntr.addChild(this.title);

        this.activity_loadCntr.addChild(this.difficulty_seekbar_bg);
        this.activity_loadCntr.addChild(this.difficulty_seekbar_easy);
        this.activity_loadCntr.addChild(this.difficulty_seekbar_medium);
        this.activity_loadCntr.addChild(this.difficulty_seekbar_hard);
        this.activity_loadCntr.addChild(this.difficulty_text);

        this.activity_loadCntr.addChild(this.startbutton);
        this.activity_loadCntr.addChild(this.startbutton_pressed);
        this.activity_loadCntr.addChild(this.progressbar);

        //add all containers to canvas stage
        app.stage.addChild(this.activity_loadCntr);

    },

    fadeeffect: function(){

      var rendererOptions = {
          antialiasing: false,
          transparent: false,
          resolution: window.devicePixelRatio,
          autoResize: true,
          backgroundColor: 0x000000
        }


        this.instructionCntr.alpha = 0;

        pk = this;
        animate();

        function animate() {

              pk.activity_loadCntr.alpha -= .02;
              pk.instructionCntr.alpha += .02;

            requestAnimationFrame(animate);


        }

    },



    common_instruction: function() {

      var sound_path = 'resources/game/c_instruction.aiff'
      var sound_name = 'c_instruction'

      this.positioning_any_sprite(this.common_bg, this.xPos, this.yPos, 1);

      setTimeout(function() {activity_obj.game_sounds(sound_path, sound_name)
      },200);

        // this.fadeeffect();

        parent = this;

        setTimeout(function() {parent.activity_loadCntr.visible = false;
        },100);
        this.instructionCntr.visible = true;

        this.positioning_any_sprite(this.dialogue_square, this.xPos, this.yPos, 1);

        //alert("vannu moneiiii");

        PIXI.loader.add('resources/tile.json')

            .load(function(loader, resources) {

                sprite_animation();
            });

        parent = this;

        function sprite_animation() {

            // create an array of textures from an image path
            var frames = [];

            for (var i = 0; i <= 10; i++) {
                //var val = i < 10 ? '0' + i : i;

                // magically works since the spritesheet was loaded with the pixi loader
                frames.push(PIXI.Texture.fromFrame('preloader_anim_' + i + '.png'));
            }

            // create an AnimatedSprite (brings back memories from the days of Flash, right ?)
            parent.anim = new PIXI.extras.AnimatedSprite(frames);

            /*
             * An Animatbottom piece of clothingedSprigame_cluete inherits all the properties of a PIXI sprite
             * so you can change its position, its anchor, mask it, etc
             */
            parent.anim.x = parent.xPos-230;
            parent.anim.y = parent.yPos;
            parent.anim.anchor.set(0.5);
            parent.anim.animationSpeed = .06;
            parent.anim.scale.set(.5);
            parent.anim.play();

            app.stage.addChild(parent.anim);

        }
        this.understand_button_pressed = new PIXI.Sprite(this.understand_button_pressed_texture);

        this.understand_button_unpressed = new PIXI.Sprite(this.understand_button_unpressed_texture);

        this.positioning_any_sprite(this.understand_button_pressed, this.xPos, this.yPos + 180,1);
        this.positioning_any_sprite(this.understand_button_unpressed, this.xPos, this.yPos + 180,1);

        this.make_interactivebutton(this.understand_button_unpressed);

        this.hover_effect(this.understand_button_pressed, this.understand_button_unpressed);



        this.understand_button_unpressed.click = function() {

            var sound_path = 'resources/common/tuk_1.mp3'
            var sound_name = 'common_instruction'
            parent.game_sounds(sound_path, sound_name);

            parent.activity_clue();
            parent.anim.visible = false;

        }

        //common instruction container
        this.instructionCntr.addChild(this.common_bg);
        this.instructionCntr.setChildIndex(this.common_bg,0);

        this.instructionCntr.addChild(this.dialogue_square);
        this.instructionCntr.setChildIndex(this.dialogue_square,1);

        this.instructionCntr.addChild(this.understand_button_pressed);
        this.instructionCntr.addChild(this.understand_button_unpressed);

        app.stage.addChild(this.instructionCntr);
        this.display_maskingcntr();

    },

    activity_clue: function() {

        this.activity_clueCntr.visible = true;
        this.instructionCntr.visible = false;

        this.button_pressed = new PIXI.Sprite(this.understand_button_pressed_texture);

        this.button_unpressed = new PIXI.Sprite(this.understand_button_unpressed_texture);

        this.positioning_any_sprite(this.dialogue_square1, this.xPos, this.yPos, 0.9);
        this.positioning_any_sprite(this.rule_message, this.xPos-20, this.yPos-170, 1);

        this.positioning_any_sprite(this.button_pressed, this.xPos, this.yPos + 180,1);
        this.positioning_any_sprite(this.button_unpressed, this.xPos, this.yPos + 180,1);
        this.make_interactivebutton(this.button_unpressed);
        this.hover_effect(this.button_pressed, this.button_unpressed);

        activity_robbery_color_red = new PIXI.Text("ചുവപ്പ്" ,{ fill: "black", font: "bold 26px Chilanka" });
        activity_robbery_color_blue = new PIXI.Text("നീല",{ fill: "black", font: "bold 26px Chilanka" });
        activity_robbery_color_grey = new PIXI.Text("ഗ്രേയ്‌",{ fill: "black", font: "bold 26px Chilanka" });
        activity_robbery_color_yellow = new PIXI.Text("മഞ്ഞ",{ fill: "black", font: "bold 26px Chilanka" });
        activity_robbery_color_brown = new PIXI.Text("ബ്രൗൺ",{ fill: "black", font: "bold 26px Chilanka" });
        activity_robbery_color_green = new PIXI.Text("പച്ച",{ fill: "black", font: "bold 26px Chilanka" });

        this.color_array = [];

        this.color_array.push(activity_robbery_color_red);
        this.color_array.push(activity_robbery_color_blue);
        this.color_array.push(activity_robbery_color_grey);
        this.color_array.push(activity_robbery_color_yellow);
        this.color_array.push(activity_robbery_color_brown);
        this.color_array.push(activity_robbery_color_green);

        //activity clue container
        this.activity_clueCntr.addChild(this.common_bg);
        this.activity_clueCntr.addChild(this.dialogue_square1);
        this.activity_clueCntr.addChild(this.button_pressed);
        this.activity_clueCntr.addChild(this.button_unpressed);
        this.activity_clueCntr.addChild(this.rule_message);
        app.stage.addChild(this.activity_clueCntr);
        this.display_maskingcntr();


        parent = this;
        //this.num = Math.floor(Math.random() * 7);
        this.num = this.random_number_generator(this.color_array.length);

        this.num1 = this.random_number_generator(this.color_array.length);

//if the random numbers for upper and lower color is same
        if(this.num1 == this.num){

           if(this.num1 == this.color_array.length-1){

             this.num1 = this.num1-1;

                  }
          else if(this.num1 == 0) {

            this.num1 = this.num1+1;

          }

          else {

            this.num1 = this.num1+1;

          }

            }

      // console.log(this.task_level)

        this.gen_num = this.random_number_generator(2);

//tasklevel_obj.mediumLevel();
        if(this.task_level == 1) {
        tasklevel_obj.easyLevel();
       }

        else if (this.task_level == 2) {
          tasklevel_obj.mediumLevel();
        }

        else if (this.task_level == 3) {
          tasklevel_obj.hardLevel();

        }


        this.button_unpressed.click = function() {

            var sound_path = 'resources/common/tuk_1.mp3'
            var sound_name = 'easy'
            parent.game_sounds(sound_path, sound_name);

            findrobber_obj.screen_setup();
            parent.activity_clueCntr.visible = false;

        }


    },

    change_same_index: function(number1, number2){

      if(number2 == number1){

         if(number2 == this.color_array.length-1){

           number2 = number2-1;

                }
        else if(number2 == 0) {

          number2 = number2+1;

        }

        else {

          number2 = number2+1;

        }

        return number1, number2;

          }

    },




    random_number_generator: function(max){

      return Math.floor(Math.random() * max );
      //return (num === 8 || num === 15) ? generateRandom(min, max) : num;

    },

    display_maskingcntr: function() {

      //masking container positioning
        this.positioning_any_sprite(this.maskingSprite, this.xPos, this.yPos, 1);

        this.maskingCntr.addChild(this.maskingSprite);
        // this.maskingCntr.setChildIndex(this.maskingSprite, 0);
        app.stage.addChild(this.maskingCntr);

    },

    activity_progressbar: function(width){

      console.log("vannu")

     this.progressbarCtr = new PIXI.Container();

    this.bar = new PIXI.Graphics();

    this.bar.position.set(activity_obj.xPos-240, activity_obj.yPos + 110);

    var i = 0;
    p = this;
    app.ticker.add(function() {

      if(i<(width)){
    p.bar.beginFill(0x70b613);
    p.bar.drawRect(0, 0,i, 20);
    p.bar.endFill();
    i+=3;
     }

     });


    this.progressbarCtr.addChild(this.bar);
    app.stage.addChild(this.progressbarCtr);


  },

  game_sounds: function(sound_path, sound_name){

    PIXI.sound.add(sound_name, sound_path);
    PIXI.sound.play(sound_name);

  }
}
