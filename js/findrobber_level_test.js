inheritPrototype(FindRobber, PIXI.Sprite);

inheritPrototype(FindRobber, PIXI.Application);

function FindRobber() {

    this.gamePlayCntr = new PIXI.Container();

    this.findRobberCtr = new PIXI.Container();

    this.trofy_sceneCntr = new PIXI.Container();
    this.trofy_sceneCntr.visible = false;


    this.thief_coming = false; //for hardLevel

    this.gender_array = [];

    this.m_head_array = [];
    this.w_head_array = [];

    this.m_hair_array = [];
    this.w_hair_array = [];

    this.m_character_array_upper = [];
    this.m_character_array_lower = [];
    this.w_character_array_upper = [];
    this.w_character_array_lower = [];

    this.character_array = [];



    this.currect_count = 0;

    this.iteration_n = 40;

    this.exit_count = 0;

    //game scene setuping assets

    this.door_background = new PIXI.Sprite(PIXI.Texture.fromImage('resources/common/gamescreen/door_background.png'));



    //response of clicking on the character
    this.arrestedSprite = new PIXI.Sprite(PIXI.Texture.fromImage('resources/common/gamescreen/arrested.png'));

    this.innocenceSprite = new PIXI.Sprite(PIXI.Texture.fromImage('resources/common/gamescreen/innocence.png'));

    this.thiefSprite = new PIXI.Sprite(PIXI.Texture.fromImage('resources/common/gamescreen/thief.png'));
    this.thiefTextSprite = new PIXI.Sprite(PIXI.Texture.fromImage('resources/common/gamescreen/thief_title.png'));

    //gender array

    this.gender_array = ["m", "w"];

    //for men

    this.m_head_array[0] = PIXI.Texture.fromImage('resources/game/character/m_head1.png');
    this.m_head_array[1] = PIXI.Texture.fromImage('resources/game/character/m_head2.png');

    this.m_hair_array[0] = PIXI.Texture.fromImage('resources/game/character/m_hair_black.png');
    this.m_hair_array[1] = PIXI.Texture.fromImage('resources/game/character/m_hair_brown.png');

    this.m_character_array_upper[0] = PIXI.Texture.fromImage('resources/game/character/m_shirt_red.png');
    this.m_character_array_upper[1] = PIXI.Texture.fromImage('resources/game/character/m_shirt_blue.png');
    this.m_character_array_upper[2] = PIXI.Texture.fromImage('resources/game/character/m_shirt_grey.png');
    this.m_character_array_upper[3] = PIXI.Texture.fromImage('resources/game/character/m_shirt_yellow.png');
    this.m_character_array_upper[4] = PIXI.Texture.fromImage('resources/game/character/m_shirt_brown.png');
    this.m_character_array_upper[5] = PIXI.Texture.fromImage('resources/game/character/m_shirt_green.png');

    this.m_character_array_upper[6] = PIXI.Texture.fromImage('resources/game/character/m_sweater_red.png');
    this.m_character_array_upper[7] = PIXI.Texture.fromImage('resources/game/character/m_sweater_blue.png');
    this.m_character_array_upper[8] = PIXI.Texture.fromImage('resources/game/character/m_sweater_grey.png');
    this.m_character_array_upper[9] = PIXI.Texture.fromImage('resources/game/character/m_sweater_yellow.png');
    this.m_character_array_upper[10] = PIXI.Texture.fromImage('resources/game/character/m_sweater_brown.png');
    this.m_character_array_upper[11] = PIXI.Texture.fromImage('resources/game/character/m_sweater_green.png');


    this.m_character_array_lower[0] = PIXI.Texture.fromImage('resources/game/character/m_pants_red.png');
    this.m_character_array_lower[1] = PIXI.Texture.fromImage('resources/game/character/m_pants_blue.png');
    this.m_character_array_lower[2] = PIXI.Texture.fromImage('resources/game/character/m_pants_grey.png');
    this.m_character_array_lower[3] = PIXI.Texture.fromImage('resources/game/character/m_pants_yellow.png');
    this.m_character_array_lower[4] = PIXI.Texture.fromImage('resources/game/character/m_pants_brown.png');
    this.m_character_array_lower[5] = PIXI.Texture.fromImage('resources/game/character/m_pants_green.png');

    this.m_character_array_lower[6] = PIXI.Texture.fromImage('resources/game/character/m_mundu_red.png');
    this.m_character_array_lower[7] = PIXI.Texture.fromImage('resources/game/character/m_mundu_blue.png');
    this.m_character_array_lower[8] = PIXI.Texture.fromImage('resources/game/character/m_mundu_grey.png');
    this.m_character_array_lower[9] = PIXI.Texture.fromImage('resources/game/character/m_mundu_yellow.png');
    this.m_character_array_lower[10] = PIXI.Texture.fromImage('resources/game/character/m_mundu_brown.png');
    this.m_character_array_lower[11] = PIXI.Texture.fromImage('resources/game/character/m_mundu_green.png');

    //for women

    this.w_head_array[0] = PIXI.Texture.fromImage('resources/game/character/f_head1.png');
    this.w_head_array[1] = PIXI.Texture.fromImage('resources/game/character/f_head2.png');

    this.w_hair_array[0] = PIXI.Texture.fromImage('resources/game/character/f_longhair_black.png');
    this.w_hair_array[1] = PIXI.Texture.fromImage('resources/game/character/f_braidhair_black.png');

    this.w_character_array_upper[0] = PIXI.Texture.fromImage('resources/game/character/f_shirt_red.png');
    this.w_character_array_upper[1] = PIXI.Texture.fromImage('resources/game/character/f_shirt_blue.png');
    this.w_character_array_upper[2] = PIXI.Texture.fromImage('resources/game/character/f_shirt_grey.png');
    this.w_character_array_upper[3] = PIXI.Texture.fromImage('resources/game/character/f_shirt_yellow.png');
    this.w_character_array_upper[4] = PIXI.Texture.fromImage('resources/game/character/f_shirt_brown.png');
    this.w_character_array_upper[5] = PIXI.Texture.fromImage('resources/game/character/f_shirt_green.png');

    this.w_character_array_upper[6] = PIXI.Texture.fromImage('resources/game/character/f_blouse_red.png');
    this.w_character_array_upper[7] = PIXI.Texture.fromImage('resources/game/character/f_blouse_blue.png');
    this.w_character_array_upper[8] = PIXI.Texture.fromImage('resources/game/character/f_blouse_grey.png');
    this.w_character_array_upper[9] = PIXI.Texture.fromImage('resources/game/character/f_blouse_yellow.png');
    this.w_character_array_upper[10] = PIXI.Texture.fromImage('resources/game/character/f_blouse_brown.png');
    this.w_character_array_upper[11] = PIXI.Texture.fromImage('resources/game/character/f_blouse_green.png');



    this.w_character_array_lower[0] = PIXI.Texture.fromImage('resources/game/character/f_pants_red.png');
    this.w_character_array_lower[1] = PIXI.Texture.fromImage('resources/game/character/f_pants_blue.png');
    this.w_character_array_lower[2] = PIXI.Texture.fromImage('resources/game/character/f_pants_grey.png');
    this.w_character_array_lower[3] = PIXI.Texture.fromImage('resources/game/character/f_pants_yellow.png');
    this.w_character_array_lower[4] = PIXI.Texture.fromImage('resources/game/character/f_pants_brown.png');
    this.w_character_array_lower[5] = PIXI.Texture.fromImage('resources/game/character/f_pants_green.png');

    this.w_character_array_lower[6] = PIXI.Texture.fromImage('resources/game/character/f_middie_red.png');
    this.w_character_array_lower[7] = PIXI.Texture.fromImage('resources/game/character/f_middie_blue.png');
    this.w_character_array_lower[8] = PIXI.Texture.fromImage('resources/game/character/f_middie_grey.png');
    this.w_character_array_lower[9] = PIXI.Texture.fromImage('resources/game/character/f_middie_yellow.png');
    this.w_character_array_lower[10] = PIXI.Texture.fromImage('resources/game/character/f_middie_brown.png');
    this.w_character_array_lower[11] = PIXI.Texture.fromImage('resources/game/character/f_middie_green.png');

    //result page

    this.dialog_square_common = PIXI.Texture.fromImage('resources/global/dialog_square_borderlines.9.png');
    this.resultsbanner = PIXI.Texture.fromImage('resources/global/resultsbanner.png');
    this.rating_trofy_disabled = PIXI.Texture.fromImage('resources/global/ratingstar_disabled.png');
    this.rating_trofy_enabled = PIXI.Texture.fromImage('resources/global/ratingstar_enabled.png');
    this.progressbar = PIXI.Texture.fromImage('resources/global/progressbar.png');

};

FindRobber.prototype = {

    constructor: FindRobber,

    screen_setup: function() {



        //for easy level character making
        if (activity_obj.task_level == 1) {

            //easy sceen background
            this.background = new PIXI.Sprite(PIXI.Texture.fromImage('resources/game/easy/background_newsstand.png'));
            this.background_bottom = new PIXI.Sprite(PIXI.Texture.fromImage('resources/game/easy/background_newsstand_bottom.png'));


            activity_obj.positioning_any_sprite(this.background, activity_obj.xPos, activity_obj.yPos, 1);
            activity_obj.positioning_any_sprite(this.door_background, activity_obj.xPos, activity_obj.yPos, 1);
            activity_obj.positioning_any_sprite(this.background_bottom, activity_obj.xPos + 160, activity_obj.yPos + 228, 1);

            num3 = activity_obj.random_number_generator(2);

            num4 = activity_obj.random_number_generator(2);

            this.duplication_count = ((this.iteration_n) / 4);

            //console.log(this.duplication_count)

            for (var i = 0; i < (this.duplication_count / 2); i++) {

                gender_num = activity_obj.random_number_generator(2);

                this.character_making(activity_obj.num, activity_obj.num1, num3, num4, gender_num);

            }


            for (var i = (this.duplication_count / 2); i < this.duplication_count; i++) {

                gender_num = activity_obj.random_number_generator(2);

                this.character_making(activity_obj.num + 6, activity_obj.num1 + 6, num3, num4, gender_num);

            }


            for (var i = this.duplication_count; i < this.iteration_n; i++) {

                this.parameter_generation();

                if ((num1 == activity_obj.num && num2 == activity_obj.num1) || (num1 == activity_obj.num + 6 && num2 == activity_obj.num1) || (num1 == activity_obj.num && num2 == activity_obj.num1 + 6) || (num1 == activity_obj.num + 6 && num2 == activity_obj.num1 + 6)) {


                } else {

                    this.character_making(num1, num2, num3, num4, gender_num);

                }
            }

        }

        //for medium level charecter making
        else if (activity_obj.task_level == 2) {


            //medium sceen background
            this.background = new PIXI.Sprite(PIXI.Texture.fromImage('resources/game/medium/background_jewellery.png'));
            this.background_bottom = new PIXI.Sprite(PIXI.Texture.fromImage('resources/game/medium/background_jewellery_bottom.png'));

            activity_obj.positioning_any_sprite(this.background, activity_obj.xPos, activity_obj.yPos, 1);
            activity_obj.positioning_any_sprite(this.door_background, activity_obj.xPos, activity_obj.yPos, 1);
            activity_obj.positioning_any_sprite(this.background_bottom, activity_obj.xPos + 160, activity_obj.yPos + 230, 1);


            this.duplication_count = ((this.iteration_n) / 4);

            var a = tasklevel_obj.uppercloth_index;

            var b = tasklevel_obj.lowercloth_index;

            this.med_l_uppercloth_index = activity_obj.num + (a * 6);
            this.med_l_lowercloth_index = activity_obj.num1 + (b * 6);

            for (var i = 0; i < (this.duplication_count); i++) {

                this.character_making(activity_obj.num + (a * 6), activity_obj.num1 + (b * 6), activity_obj.gen_num, activity_obj.gen_num, activity_obj.gen_num);

            }

            //for creating same color dresses

            for (var i = this.duplication_count; i < (2 * (this.duplication_count)); i++) {

                this.parameter_generation();

                if ((num1 != this.med_l_uppercloth_index) || (num2 != this.med_l_lowercloth_index))

                    this.character_making(num1, num2, num3, num4, gender_num);

            }

            //for creating all color dresses

            for (var i = 2 * (this.duplication_count); i < (this.iteration_n + 1); i++) {

                this.parameter_generation();

                if ((num1 != this.med_l_uppercloth_index) || (num2 != this.med_l_lowercloth_index))

                    this.character_making(num1, num2, num3, num4, gender_num);


            }

        }



        //for hard level
        else if (activity_obj.task_level == 3) {

            //hard sceen background
            this.background = new PIXI.Sprite(PIXI.Texture.fromImage('resources/game/hard/background_bank.png'));
            this.background_bottom = new PIXI.Sprite(PIXI.Texture.fromImage('resources/game/hard/background_bank_bottom.png'));

            activity_obj.positioning_any_sprite(this.background, activity_obj.xPos, activity_obj.yPos, 1);
            activity_obj.positioning_any_sprite(this.door_background, activity_obj.xPos, activity_obj.yPos, 1);
            activity_obj.positioning_any_sprite(this.background_bottom, activity_obj.xPos + 160, activity_obj.yPos + 235, 1);


            num3 = activity_obj.random_number_generator(2);

            num4 = activity_obj.random_number_generator(2);

            this.duplication_count = ((this.iteration_n) / 4);

            //console.log(this.duplication_count)

            for (var i = 0; i < this.duplication_count; i++) {

                gender_num = activity_obj.random_number_generator(2);

                this.character_making(activity_obj.num, activity_obj.num1, num3, num4, gender_num);

            }

            for (var i = this.duplication_count; i < (this.iteration_n + 1); i++) {

                this.parameter_generation();

                if ((num1 == activity_obj.num && num2 == activity_obj.num1) || (num1 == activity_obj.num + 6 && num2 == activity_obj.num1) || (num1 == activity_obj.num && num2 == activity_obj.num1 + 6) || (num1 == activity_obj.num + 6 && num2 == activity_obj.num1 + 6)) {


                } else {

                    this.character_making(num1, num2, num3, num4, gender_num);

                }


            }

        }

        this.display_everything();

    },

    parameter_generation: function() {

        num1 = activity_obj.random_number_generator(this.m_character_array_upper.length);

        num2 = activity_obj.random_number_generator(this.m_character_array_upper.length);

        num3 = activity_obj.random_number_generator(2);

        num4 = activity_obj.random_number_generator(2);

        gender_num = activity_obj.random_number_generator(2);

    },

    character_making: function(n1, n2, n3, n4, gender_num) {

        this['charecterCtr'] = new PIXI.Container();

        //check male or female
        if (this.gender_array[gender_num] == "m") {

            var upperSprite = new PIXI.Sprite(this.m_character_array_lower[n2]);

            var lowerSprite = new PIXI.Sprite(this.m_character_array_upper[n1]);

            var headSprite = new PIXI.Sprite(this.m_head_array[n3]);

            var hairSprite = new PIXI.Sprite(this.m_hair_array[n4]);
        } else if (this.gender_array[gender_num] == "w") {

            var upperSprite = new PIXI.Sprite(this.w_character_array_lower[n2]);

            var lowerSprite = new PIXI.Sprite(this.w_character_array_upper[n1]);

            var headSprite = new PIXI.Sprite(this.w_head_array[n3]);

            var hairSprite = new PIXI.Sprite(this.w_hair_array[n4]);
        }

        this['charecterCtr'].addChild(upperSprite);
        this['charecterCtr'].addChild(lowerSprite);
        this['charecterCtr'].addChild(headSprite);
        this['charecterCtr'].addChild(hairSprite);

        activity_obj.positioning_any_sprite(headSprite, 150, 0, 1);
        activity_obj.positioning_any_sprite(hairSprite, 150, 35, 1);
        activity_obj.positioning_any_sprite(lowerSprite, 150, 120, 1);
        activity_obj.positioning_any_sprite(upperSprite, 150, 230, 1);

        this.character_array.push(this['charecterCtr']);


    },

    display_everything: function() {

        this.graphics_rectangle = new PIXI.Graphics();

        this.findRobberCtr.addChild(this.door_background);

        this.findRobberCtr.addChild(this.graphics_rectangle);

        this.findRobberCtr.addChild(this.background_bottom);

        pick = activity_obj.random_number_generator(this.character_array.length);


        this.character_array[pick].position.set(activity_obj.xPos + 250, activity_obj.yPos - 100);

        // this.character_array[pick].scale.set(.5);

        this.findRobberCtr.addChild(this.character_array[pick]);

        activity_obj.make_interactivebutton(this.character_array[pick])


        this.findRobberCtr.addChild(this.background);

        this.gamePlayCntr.addChild(this.findRobberCtr);

        app.stage.addChild(this.gamePlayCntr);

        activity_obj.display_maskingcntr();

        this.character_movement(pick);


    },


    function_moving:function(nc ,xpos_v1, xpos_v2, duration) {

      nc.scale.set(.95)

      var renderer = new PIXI.autoDetectRenderer(800, 600);
        document.body.appendChild(renderer.view);
        var stage = new PIXI.Container();

      var raf;


          function animate(){
            raf = window.requestAnimationFrame(animate);
            renderer.render(stage);
            PIXI.tweenManager.update();
          }

          function stop(){
            window.cancelAnimationFrame(raf);
          }

          //Create two tweens to use differents easings in differentes axis
          var tweenX = PIXI.tweenManager.createTween(nc);

          var TIME = duration;

          function show(easing){

            //clear the tweens
            tweenX.stop().clear();

            //Horizontal movement
            tweenX.time = TIME;

            if(xpos_v1 == activity_obj.xPos + 200){
            tweenX.easing = PIXI.tween.Easing.outQuad();
            }
            else {
              tweenX.easing = PIXI.tween.Easing.inQuad();
            }

            tweenX.from({

              x: xpos_v1

            });

            tweenX.to({

              x: xpos_v2

            });

            tweenX.loop = false;

            tweenX.start();

          }

          show(PIXI.tween.Easing.linear());
          animate();


    },


    character_movement: function(n) {

        this.total_point = this.currect_count - this.exit_count;

        if (this.total_point <= 49) {

          console.log(this.currect_count)

            // 600 , 450 ,350
            if (this.total_point <=20)
               var time_var = 600;
            else if ((this.total_point >= 20) && (this.total_point < 30))
              var time_var = 500;
            else if ((this.total_point >= 30) && (this.total_point < 40))
              var time_var = 400;
            else if ((this.total_point >= 40) && (this.total_point <=50))
              var time_var = 350;

            paren = this;

            var dude = this.character_array[n];

            // app.stop();

            setTimeout(function() {

                //app.ticker.remove(movingleft);
              //  paren.function_moving(dude, 1200, 970 ,time_var);
                paren.function_moving(dude, activity_obj.xPos + 200, activity_obj.xPos+20  ,time_var);

            }, time_var/2);
            // this.function_moving(dude, 1200, 970 ,600);


            setTimeout(function() {

                //app.ticker.remove(movingleft);
                paren.flagforthiefclick = false;

            }, time_var);



            if (activity_obj.task_level == 1) {

                console.log(pick)
                this.currect_count += 1;

                //clicking function for character
                this.character_array[pick].click = function() {

                    paren.flagforthiefclick = true; //if we click on the character ,then no need to show the thief indication
                    //if we click the theif
                    if (pick < (paren.duplication_count)) {

                        var sound_path = 'resources/common/chains.mp3'
                        var sound_name = 'e_currect'
                        activity_obj.game_sounds(sound_path, sound_name);

                        paren.reaction_popup(paren.arrestedSprite, activity_obj.xPos - 200, activity_obj.yPos - 80, .7);
                        draw_reaction_color(activity_obj.xPos, activity_obj.yPos - 200, 0x66ff66);

                        // paren.currect_count += 1;

                    } else {

                        var sound_path = 'resources/common/error.mp3'
                        var sound_name = 'e_wrong'
                        activity_obj.game_sounds(sound_path, sound_name);

                        paren.reaction_popup(paren.innocenceSprite, activity_obj.xPos - 200, activity_obj.yPos - 80, .7);
                        draw_reaction_color(activity_obj.xPos, activity_obj.yPos - 200, 0xff3333);

                        paren.exit_count += 1;

                        if (paren.exit_count == 3) {

                            setTimeout(function() {

                                paren.trofy_board_scene();

                            }, 1500);

                        }

                    }

                    function draw_reaction_color(xpos, ypos, clr) {

                        paren.graphics_rectangle.beginFill(clr);
                        // draw a rectangle
                        paren.graphics_rectangle.drawRect(xpos, ypos, 400, 500);

                    }


                }
            } else if (activity_obj.task_level == 2) {

                this.currect_count += 1;
                //clicking function for character
                this.character_array[pick].click = function() {


                    paren.flagforthiefclick = true; //if we click on the character ,then no need to show the thief indication
                    //if we click the theif
                    if (pick < (paren.duplication_count)) {

                        var sound_path = 'resources/common/chains.mp3'
                        var sound_name = 'm_currect'
                        activity_obj.game_sounds(sound_path, sound_name);

                        paren.reaction_popup(paren.arrestedSprite, activity_obj.xPos - 200, activity_obj.yPos - 80, .7);
                        draw_reaction_color(activity_obj.xPos, activity_obj.yPos - 200, 0x66ff66);

                        // paren.currect_count += 1;


                    } else {

                        var sound_path = 'resources/common/error.mp3'
                        var sound_name = 'm_wrong'
                        activity_obj.game_sounds(sound_path, sound_name);

                        paren.reaction_popup(paren.innocenceSprite, activity_obj.xPos - 200, activity_obj.yPos - 80, .7);
                        draw_reaction_color(activity_obj.xPos, activity_obj.yPos - 200, 0xff3333);

                        paren.exit_count += 1;

                        if (paren.exit_count == 3) {

                            setTimeout(function() {

                                paren.trofy_board_scene();

                            }, 1500);

                        }

                    }

                    function draw_reaction_color(xpos, ypos, clr) {

                        paren.graphics_rectangle.beginFill(clr);
                        // draw a rectangle
                        paren.graphics_rectangle.drawRect(xpos, ypos, 400, 500);

                    }


                }

            }


            //level hard
            if (activity_obj.task_level == 3) {

                this.currect_count += 1;
                //  console.log(this.thief_coming)
                //clicking function for character
                this.character_array[pick].click = function() {

                    paren.flagforthiefclick = true;
                    //if we click the theif
                    if (paren.thief_coming == true) {

                        //console.log(paren.thief_coming);

                        var sound_path = 'resources/common/chains.mp3'
                        var sound_name = 'h_currect'
                        activity_obj.game_sounds(sound_path, sound_name);

                        paren.reaction_popup(paren.arrestedSprite, activity_obj.xPos - 200, activity_obj.yPos - 80, .7);
                        draw_reaction_color(activity_obj.xPos, activity_obj.yPos - 200, 0x66ff66);

                        // paren.currect_count += 1;
                        paren.thief_coming = false;

                    } else {

                        var sound_path = 'resources/common/error.mp3'
                        var sound_name = 'h_wrong'
                        activity_obj.game_sounds(sound_path, sound_name);

                        paren.reaction_popup(paren.innocenceSprite, activity_obj.xPos - 200, activity_obj.yPos - 80, .7);
                        draw_reaction_color(activity_obj.xPos, activity_obj.yPos - 200, 0xff3333);

                        paren.exit_count += 1;

                        if (paren.exit_count == 3) {

                            setTimeout(function() {

                                paren.trofy_board_scene();

                            }, 1500);

                        }

                        paren.thief_coming = false;

                    }

                    function draw_reaction_color(xpos, ypos, clr) {

                        paren.graphics_rectangle.beginFill(clr);
                        // draw a rectangle
                        paren.graphics_rectangle.drawRect(xpos, ypos, 400, 500);

                    }


                }
            }


            setTimeout(function() {

                // app.ticker.add(movingright);
                paren.function_moving(dude, activity_obj.xPos+20, activity_obj.xPos-500 , time_var);


            }, time_var * 3);



setTimeout(function() {

            if (activity_obj.task_level == 3) {


                if ((paren.thief_coming == true) && (paren.flagforthiefclick == false)) {

                    var sound_path = 'resources/common/alarm.mp3'
                    var sound_name = 'hard_miss'
                    activity_obj.game_sounds(sound_path, sound_name);

                    paren.reaction_popup(paren.thiefSprite, activity_obj.xPos - 200, activity_obj.yPos - 80, .7);
                    paren.exit_count += 1;

                    if (paren.exit_count == 3) {

                        setTimeout(function() {

                            paren.trofy_board_scene();

                        }, 1500);

                    }

                }
            } else {

                if ((pick < (paren.duplication_count) && (paren.flagforthiefclick == false))) {

                    var sound_path = 'resources/common/alarm.mp3'
                    var sound_name = 'e_m_miss'
                    activity_obj.game_sounds(sound_path, sound_name);

                    paren.reaction_popup(paren.thiefSprite, activity_obj.xPos - 200, activity_obj.yPos - 80, .7);
                    paren.exit_count += 1;

                    if (paren.exit_count == 3) {

                        setTimeout(function() {

                            paren.trofy_board_scene();

                        }, 1500);

                    }

                }


            }


  }, time_var * 3 + 400);





            setTimeout(function() {

                // /app.ticker.remove(movingright);

                //for the case of hard level

                if ((activity_obj.task_level == 3) && (pick < (paren.duplication_count))) {

                    paren.thief_coming = true;

                } else {

                    paren.thief_coming = false;

                }

                paren.findRobberCtr.removeChild(dude);
                if (paren.exit_count < 3)
                    paren.display_everything();


            }, time_var * 4 +500);


            // setTimeout(function() {
            //
            //
            //
            //
            // }, time_var * 4);


        } else {

            this.trofy_board_scene();
        }


    },



    reaction_popup: function(sprite, x, y, s) {

        activity_obj.positioning_any_sprite(sprite, x, y, s);
        this.findRobberCtr.addChild(sprite);

    },

    trofy_board_scene: function() {

        findrobber_obj.findRobberCtr.visible = false;
        this.trofy_sceneCntr.visible = true;

        //total_point = this.currect_count - this.exit_count;

        currect_count_text = new PIXI.Text(this.total_point, {
            fill: "black",
            font: "bold 22px Chilanka"
        });

        currect_count_commenttext = new PIXI.Text("      /50 പ്രാവശ്യം നിങ്ങൾ ശരിയായ തീരുമാനം എടുത്തു", {
            fill: "grey",
            font: "bold 22px Chilanka"
        });

        nill_text = new PIXI.Text("", {

            fill: "grey",
            font: "bold 22px Chilanka"

        });

        onestar_commenttex = new PIXI.Text("താങ്കൾ നന്നായി തന്നെ കളിച്ചു്", {

            fill: "grey",
            font: "bold 22px Chilanka"

        });

        twostar_commenttext = new PIXI.Text("ഇത്തവണ താങ്കൾ വളരെ നന്നായി കളിച്ചു", {

            fill: "grey",
            font: "bold 22px Chilanka"
        });

        threestar_commenttext = new PIXI.Text("താങ്കളൊരു സമർഥനായ ഡിറ്റക്റ്റീവ് ആണെന്ന് തെളിയിച്ചു", {

            fill: "grey",
            font: "bold 22px Chilanka"
        });


        dialog_square = new PIXI.Sprite(this.dialog_square_common);
        resultsbanner_sprite = new PIXI.Sprite(this.resultsbanner);
        progressbar_sprite = new PIXI.Sprite(this.progressbar);
        stats_sprite = new PIXI.Sprite(activity_obj.stats_texture);

        green_button_pressed = new PIXI.Sprite(activity_obj.understand_button_pressed_green_texture);
        green_button_unpressed = new PIXI.Sprite(activity_obj.understand_button_unpressed_green_texture);
        blue_button_pressed = new PIXI.Sprite(activity_obj.understand_button_pressed_blue_texture);
        blue_button_unpressed = new PIXI.Sprite(activity_obj.understand_button_unpressed_blue_texture);

        activity_obj.positioning_any_sprite(dialog_square, activity_obj.xPos, activity_obj.yPos, 1.8);
        activity_obj.positioning_any_sprite(resultsbanner_sprite, activity_obj.xPos, activity_obj.yPos - 200, .6);
        //statistic bar
        activity_obj.positioning_any_sprite(stats_sprite, activity_obj.xPos - 285, activity_obj.yPos + 75, 1);

        activity_obj.positioning_any_sprite(green_button_pressed, activity_obj.xPos + 150, activity_obj.yPos + 200, 1);
        activity_obj.positioning_any_sprite(green_button_unpressed, activity_obj.xPos + 150, activity_obj.yPos + 200, 1);
        activity_obj.positioning_any_sprite(activity_obj.play_again_text, activity_obj.xPos + 150, activity_obj.yPos + 190, 1);

        activity_obj.hover_effect(green_button_pressed, green_button_unpressed);
        activity_obj.make_interactivebutton(green_button_unpressed);

        activity_obj.positioning_any_sprite(blue_button_pressed, activity_obj.xPos - 150, activity_obj.yPos + 200, 1);
        activity_obj.positioning_any_sprite(blue_button_unpressed, activity_obj.xPos - 150, activity_obj.yPos + 200, 1);
        activity_obj.positioning_any_sprite(activity_obj.exit_text, activity_obj.xPos - 150, activity_obj.yPos + 190, 1);

        // thief counting text positioning
        activity_obj.positioning_any_sprite(currect_count_text, activity_obj.xPos - 245, activity_obj.yPos + 75, 1);
        activity_obj.positioning_any_sprite(currect_count_commenttext, activity_obj.xPos + 10, activity_obj.yPos + 75, 1);

        //button for quit
        activity_obj.hover_effect(blue_button_pressed, blue_button_unpressed);
        activity_obj.make_interactivebutton(blue_button_unpressed);

        this.trofy_sceneCntr.addChild(activity_obj.common_bg);
        this.trofy_sceneCntr.setChildIndex(activity_obj.common_bg, 0);

        this.trofy_sceneCntr.addChild(dialog_square);
        this.trofy_sceneCntr.setChildIndex(dialog_square, 1);

        this.trofy_sceneCntr.addChild(resultsbanner_sprite);
        this.trofy_sceneCntr.setChildIndex(resultsbanner_sprite, 2);
        this.trofy_sceneCntr.addChild(stats_sprite);
        this.trofy_sceneCntr.setChildIndex(stats_sprite, 3);


        this.trofy_sceneCntr.addChild(currect_count_text);
        this.trofy_sceneCntr.addChild(currect_count_commenttext);

        this.trofy_sceneCntr.addChild(green_button_pressed);
        this.trofy_sceneCntr.addChild(green_button_unpressed);
        this.trofy_sceneCntr.addChild(activity_obj.play_again_text);

        this.trofy_sceneCntr.addChild(blue_button_pressed);
        this.trofy_sceneCntr.addChild(blue_button_unpressed);
        this.trofy_sceneCntr.addChild(activity_obj.exit_text);

        prnt = this;

        trophy_displaying(this.rating_trofy_disabled, 3, nill_text, 0, 0);

        if (this.total_point < 20) {

            var sound_path = 'resources/game/zerostar.aiff'
            var sound_name = 'zerostar'

            setTimeout(function() {
                activity_obj.game_sounds(sound_path, sound_name)
            }, 3000);

        } else if ((this.total_point >= 20) && (this.total_point < 30)) {

            var sound_path = 'resources/game/onestar.aiff'
            var sound_name = 'onestar'

            setTimeout(function() {
                activity_obj.game_sounds(sound_path, sound_name)
            }, 3000);


            trophy_displaying(prnt.rating_trofy_enabled, 1, onestar_commenttex, 1000, 160);


        } else if ((this.total_point >= 30) && (this.total_point < 40)) {

            var sound_path = 'resources/game/twostar.aiff'
            var sound_name = 'twostar'

            setTimeout(function() {
                activity_obj.game_sounds(sound_path, sound_name)
            }, 3500);

            trophy_displaying(prnt.rating_trofy_enabled, 2, twostar_commenttext, 1000, 320);

        } else if ((this.total_point >= 40) && (this.total_point <=50)) {

            var sound_path = 'resources/game/threestar.aiff'
            var sound_name = 'threestar'

            setTimeout(function() {
                activity_obj.game_sounds(sound_path, sound_name)
            }, 4000);

            trophy_displaying(prnt.rating_trofy_enabled, 3, threestar_commenttext, 1000, 480);

        }


        function trophy_displaying(trofy, t_counter, victory_text, delay, p_bar_width) {

            var sound_path = 'resources/common/victoryscreen_show.mp3'
            var sound_name = 'victory'


            activity_obj.game_sounds(sound_path, sound_name);

            prnt.trofy_sceneCntr.addChild(victory_text);
            //victory text positioning
            activity_obj.positioning_any_sprite(victory_text, activity_obj.xPos, activity_obj.yPos, 1);


            setTimeout(function() {

                for (var i = 0; i < t_counter; i++) {

                    rating_trofy_sprite = new PIXI.Sprite(trofy);
                    activity_obj.positioning_any_sprite(rating_trofy_sprite, activity_obj.xPos - 100 + (i * 100), activity_obj.yPos - 100, 0.8);
                    prnt.trofy_sceneCntr.addChild(rating_trofy_sprite);

                }

            }, delay);

            setTimeout(function() {

                activity_obj.activity_progressbar(p_bar_width);
                prnt.trofy_sceneCntr.addChild(activity_obj.progressbarCtr);
                prnt.trofy_sceneCntr.setChildIndex(activity_obj.progressbarCtr, 4);

            }, delay + 1000);

        }

        this.trofy_sceneCntr.addChild(progressbar_sprite);
        this.trofy_sceneCntr.setChildIndex(progressbar_sprite, 5);

        activity_obj.positioning_any_sprite(progressbar_sprite, activity_obj.xPos, activity_obj.yPos + 120, 1);
        this.gamePlayCntr.addChild(this.trofy_sceneCntr);

        //press the quit button
        blue_button_unpressed.click = function() {

            prnt.gamePlayCntr.visible = false;
            location.reload(true);

        }

        //press the quit button
        green_button_unpressed.click = function() {

            prnt.gamePlayCntr.visible = false;
            location.reload(true);

        }

    },

}
